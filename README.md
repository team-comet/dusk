# Dusk #

A simple and open source CMS in ASP .NET Core available for usage by Comet, but eventually other projects / databases.


### How do I get set up for development? ###

If you have Visual Studio 2017 (for Mac or Windows) as long as you have it up to date and the ASP .NET Core components installed you should just be able to open the solution.

If you don't have Visual Studio and rather not download all that bloat follow these steps instead:

You must download and install .NET Core SDK 2.1.103 or latest from here: 

https://www.microsoft.com/net/download/

Then you must download Visual Studio Code (this is **nothing** like Visual Studio 2017 it is significantly smaller) and install it (Linux, Mac and Windows supported).

Make sure to install the C# plugin if not already installed.

Open the source with Visual Studio Code (File -> Open Folder) and open it in the main directory, so you should be able to see: Dusk.sln, LICENSE, and README.md (this file) all in there. If you rather hide the parent files, you can go into the Dusk subdirectory instead.

### How do I get set up for testing? ###

For now just follow the same route as the development setup.

### How do I get set up for deployment? ###

This project is too early for this.

### Contribution guidelines ###

If you wish to contribute the first step is to find a reported "Issue" to work on, whether a bug or feature request. 
Once you've found your bug, you must create a new branch from the "Development" branch named after the ticket number 
(see existing branches for guidance or contact me for help). Once you believe all your changes are done, you can commit
and push those changes and then send a PR (Pull Request) of your changes from your branch into Development. They will
be peer reviewed and approved accordingly.

### Who do I talk to? ###

* Moogly on here, or RaGEZONE.
* Also available via project Discord here: https://discord.gg/74fNU6u